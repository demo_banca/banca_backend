# Demo Banca (Backend)

## Technologies

* Spring Boot 1.5.1
* Spring Data 1.5.1
* Java 1.8
* Gradle
* JUnit 4.12
* MongoDB 3.4
* Apache Tomcat 8.0.27
* Swagger 2.8.0

## Note

It is necessary to execute the "createNewDepartments()" tests of the class "DepartmentTests.java".

## Authors

* **Eng. Diego Huamanchahua**