package com.banca.banca_backend.form.domain;

import com.banca.banca_backend.form.bean.ProvinceBean;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Department {

    @Id
    @Ignore
    public String _id;

    @ApiModelProperty(notes = "Name of the department")
    private String name;

    @ApiModelProperty(notes = "List of provinces")
    private List<ProvinceBean> provinces;

    @ApiModelProperty(notes = "Creation date (Audit Field)")
    private Long creationDate;

    @ApiModelProperty(notes = "Creation user (Audit Field)")
    private String creationUser;

    @ApiModelProperty(notes = "Modification date (Audit Field)")
    private Long modificationDate;

    @ApiModelProperty(notes = "Modification user (Audit Field)")
    private String modificationUser;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProvinceBean> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<ProvinceBean> provinces) {
        this.provinces = provinces;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Long getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Long modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationUser() {
        return modificationUser;
    }

    public void setModificationUser(String modificationUser) {
        this.modificationUser = modificationUser;
    }
}
