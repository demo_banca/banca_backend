package com.banca.banca_backend.form.domain;

import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Person {

    @Id
    @Ignore
    public String _id;

    @ApiModelProperty(notes = "Name of the person")
    private String name;

    @ApiModelProperty(notes = "Birthdate of the person")
    private Long birthdate;

    @ApiModelProperty(notes = "Department where you currently live")
    private String department;

    @ApiModelProperty(notes = "Province where you currently live")
    private String province;

    @ApiModelProperty(notes = "District where you currently live")
    private String district;

    @ApiModelProperty(notes = "Address where you currently live")
    private String address;

    @ApiModelProperty(notes = "Status of the person")
    private Boolean status;

    @ApiModelProperty(notes = "Creation date (Audit field)")
    private Long creationDate;

    @ApiModelProperty(notes = "Creation user (Audit field)")
    private String creationUser;

    @ApiModelProperty(notes = "Modification date (Audit field)")
    private Long modificationDate;

    @ApiModelProperty(notes = "Modification user (Audit field)")
    private String modificationUser;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Long birthdate) {
        this.birthdate = birthdate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Long getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Long modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationUser() {
        return modificationUser;
    }

    public void setModificationUser(String modificationUser) {
        this.modificationUser = modificationUser;
    }
}
