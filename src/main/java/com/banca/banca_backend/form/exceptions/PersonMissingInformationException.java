package com.banca.banca_backend.form.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "You must include a name")
public class PersonMissingInformationException extends RuntimeException {
}
