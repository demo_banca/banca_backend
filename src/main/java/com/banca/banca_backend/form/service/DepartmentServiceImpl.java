package com.banca.banca_backend.form.service;

import com.banca.banca_backend.form.domain.Department;
import com.banca.banca_backend.form.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public ResponseEntity<Department> createNewDepartment(Department department) {
        departmentRepository.createNewDepartment(department);
        return new ResponseEntity<Department>(department, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Department>> getAllDepartmentsResponse() {
        List<Department> departments = departmentRepository.getAllDepartments();
        return new ResponseEntity<List<Department>>(departments, HttpStatus.OK);
    }

}
