package com.banca.banca_backend.form.service;

import com.banca.banca_backend.form.domain.Department;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DepartmentService {

    ResponseEntity<Department> createNewDepartment(Department department);

    ResponseEntity<List<Department>> getAllDepartmentsResponse();

}
