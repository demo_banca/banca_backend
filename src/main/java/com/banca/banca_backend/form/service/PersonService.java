package com.banca.banca_backend.form.service;

import com.banca.banca_backend.form.domain.Person;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PersonService {

    ResponseEntity<Person> createNewContact(Person person);

    ResponseEntity<List<Person>> getAllPersonsResponse();

    ResponseEntity<Person> updatePerson(String name, Person person);

    ResponseEntity<Person> deletePerson(String name);

}
