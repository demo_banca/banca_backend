package com.banca.banca_backend.form.service;

import com.banca.banca_backend.form.domain.Person;
import com.banca.banca_backend.form.exceptions.PersonMissingInformationException;
import com.banca.banca_backend.form.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public ResponseEntity<Person> createNewContact(Person person) {
        if (person != null) {
            personRepository.createNewPerson(person);
            return new ResponseEntity<Person>(new Person(), HttpStatus.CREATED);
        } else {
            throw new PersonMissingInformationException();
        }
    }

    @Override
    public ResponseEntity<List<Person>> getAllPersonsResponse() {
        List<Person> persons = personRepository.getAllPersons();
        return new ResponseEntity<List<Person>>(persons, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> updatePerson(String name, Person person) {
        personRepository.updatePerson(name, person);
        return new ResponseEntity<Person>(new Person(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> deletePerson(String name) {
        personRepository.deletePerson(name);
        return new ResponseEntity<Person>(new Person(), HttpStatus.NO_CONTENT);
    }

}
