package com.banca.banca_backend.form.bean;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ProvinceBean {

    @ApiModelProperty(notes = "Name of the province")
    public String name;

    @ApiModelProperty(notes = "List of districts")
    public List<String> districts;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDistricts(List<String> districts) {
        this.districts = districts;
    }

    public List<String> getDistricts() {
        return districts;
    }
}