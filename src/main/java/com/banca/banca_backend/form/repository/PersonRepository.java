package com.banca.banca_backend.form.repository;

import com.banca.banca_backend.form.domain.Person;

import java.util.List;

public interface PersonRepository {

    void createNewPerson(Person person);

    List<Person> getAllPersons();

    void updatePerson(String name, Person person);

    void deletePerson(String name);

}
