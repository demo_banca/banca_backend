package com.banca.banca_backend.form.repository;

import com.banca.banca_backend.form.domain.Department;

import java.util.List;

public interface DepartmentRepository {

    void createNewDepartment(Department department);

    List<Department> getAllDepartments();

}
