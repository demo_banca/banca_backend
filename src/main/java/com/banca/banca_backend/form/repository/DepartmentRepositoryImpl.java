package com.banca.banca_backend.form.repository;

import com.banca.banca_backend.form.domain.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MongoOperations mongoOperations;

    @PostConstruct
    private void init() {
        if (!mongoOperations.collectionExists(Department.class)) {
            mongoOperations.createCollection(Department.class);
            logger.info("[DEPARTMENT] CREATE NEW DEPARTMENT COLLECTION");
        }
    }

    @Override
    public void createNewDepartment(Department department) {
        department.setCreationUser("USERNAME");
        department.setCreationDate(new Date().getTime());
        mongoOperations.insert(department);
        logger.info("[DEPARTMENT] CREATE NEW PERSON: " + department.getName());
    }

    @Override
    public List<Department> getAllDepartments() {
        List<Department> departments = mongoOperations.findAll(Department.class);
        logger.info("[DEPARTMENT] GET ALL DEPARTMENTS");
        return departments;
    }

}
