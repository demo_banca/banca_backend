package com.banca.banca_backend.form.repository;

import com.banca.banca_backend.form.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MongoOperations mongoOperations;

    @PostConstruct
    private void init() {
        if (!mongoOperations.collectionExists(Person.class)) {
            mongoOperations.createCollection(Person.class);
            logger.info("[PERSON] CREATE NEW PERSON COLLECTION");
        }
    }

    @Override
    public void createNewPerson(Person person) {
        person.setCreationUser("USERNAME");
        person.setCreationDate(new Date().getTime());
        person.setStatus(true);
        mongoOperations.insert(person);
        logger.info("[PERSON] CREATE NEW PERSON: " + person.getName());
    }

    @Override
    public List<Person> getAllPersons() {
        List<Person> persons = mongoOperations.findAll(Person.class);
        logger.info("[PERSON] GET ALL PERSONS");
        return persons;
    }

    @Override
    public void updatePerson(String name, Person person) {
        BasicQuery query = new BasicQuery("{name: '" + name + "'}");
        Person findPerson = mongoOperations.findOne(query, Person.class);
        if (findPerson != null) {
            mongoOperations.remove(findPerson);
            person.setCreationUser("USERNAME");
            person.setCreationDate(new Date().getTime());
            mongoOperations.insert(person);
            logger.info("[PERSON] UPDATE PERSON: [OLD NAME: " + name + "] - [NEW NAME: " + person.getName() + "]");
        } else {
            logger.info("[PERSON] UPDATE PERSON: [PERSON NOT FOUND: " + name + "]");
        }
    }

    @Override
    public void deletePerson(String name) {
        BasicQuery query = new BasicQuery("{name: '" + name + "'}");
        Person person = mongoOperations.findOne(query, Person.class);
        if (person != null) {
            mongoOperations.remove(person);
            logger.info("[PERSON] DELETE PERSON: " + name);
        } else {
            logger.info("[PERSON] DELETE PERSON: [PERSON NOT FOUND: " + name + "]");
        }
    }

}
