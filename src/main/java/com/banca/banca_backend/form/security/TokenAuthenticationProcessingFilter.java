package com.banca.banca_backend.form.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

@Component
public class TokenAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

    private String token = null;
    private static final String SECURITY_TOKEN_KEY = "token";

    @Autowired
    protected TokenAuthenticationProcessingFilter(@Value("/") String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
        // TODO Auto-generated constructor stub
        super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(defaultFilterProcessesUrl));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        // TODO Auto-generated method stub
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(this.token, "");
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
        SecurityContextHolder.getContext().setAuthentication(super.getAuthenticationManager().authenticate(authentication));

        return authentication;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        Authentication authResult;

//        if (request.getRequestURI().equals("/login") || request.getRequestURI().equals("/status")) {
        chain.doFilter(request, response);
//            return;
//        }

//
//        //allow pre-flight validation from jquery
//        if (request.getMethod().equalsIgnoreCase("options")) {
//            response.addHeader("Access-Control-Allow-Origin", "*");
//            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//            response.addHeader("Access-Control-Allow-Headers", "token, Content-Type");
//            return;
//        }
//
//        this.token = request.getHeader(SECURITY_TOKEN_KEY);
//
//        try {
//            authResult = attemptAuthentication(request, response);
//            if (authResult == null) {
//                // return immediately as subclass has indicated that it hasn't completed authentication
//                return;
//            }
//        } catch (InternalAuthenticationServiceException failed) {
//            logger.error("An internal error occurred while trying to authenticate the user.", failed);
//            unsuccessfulAuthentication(request, response, failed);
//            return;
//        } catch (org.springframework.security.core.AuthenticationException failed) {
//            // Authentication failed
//            unsuccessfulAuthentication(request, response, failed);
//            return;
//        }
//
//        // Authentication success
//        chain.doFilter(request, response);
//
//        successfulAuthentication(request, response, chain, authResult);

    }

    @Autowired
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }
}
