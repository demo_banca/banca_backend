package com.banca.banca_backend.form.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    public UserDetails loadUserByUsername(String token) throws UsernameNotFoundException {

        return new User(
                "ADMINISTRATOR",
                "",
                true,
                true,
                true,
                true,
                AuthorityUtils.createAuthorityList("ADMIN"));
    }

}
