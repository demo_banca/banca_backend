package com.banca.banca_backend.form.controller;

import com.banca.banca_backend.form.domain.Person;
import com.banca.banca_backend.form.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "PersonControllerAPI", description = "Show the person info", produces = "application/json; charset=utf-8")
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/persons", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ApiOperation("Create new person")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Person.class)})
    public ResponseEntity<Person> createNewPerson(@RequestBody Person person) {
        return personService.createNewContact(person);
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiOperation("Get all persons")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Person.class)})
    public ResponseEntity<List<Person>> getAllPersons() throws Throwable {
        return personService.getAllPersonsResponse();
    }

    @RequestMapping(value = "/persons/{name}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8")
    @ApiOperation("Update person")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Person.class)})
    public ResponseEntity<Person> updatePerson(@PathVariable String name, @RequestBody Person person) {
        return personService.updatePerson(name, person);
    }

    @RequestMapping(value = "/persons/{name}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
    @ApiOperation("Delete person")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Person.class)})
    public ResponseEntity<Person> deletePerson(@PathVariable String name) {
        return personService.deletePerson(name);
    }

}
