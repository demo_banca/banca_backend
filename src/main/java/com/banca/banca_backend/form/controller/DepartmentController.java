package com.banca.banca_backend.form.controller;

import com.banca.banca_backend.form.domain.Department;
import com.banca.banca_backend.form.service.DepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "DepartmentControllerAPI", description = "Show the department info", produces = "application/json; charset=utf-8")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping(value = "/departments", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ApiOperation("Create new department")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Department.class)})
    public ResponseEntity<Department> createNewDepartment(@RequestBody Department department) {
        return departmentService.createNewDepartment(department);
    }

    @RequestMapping(value = "/departments", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiOperation("Get all departments")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Department.class)})
    public ResponseEntity<List<Department>> getAllDepartments() throws Throwable {
        return departmentService.getAllDepartmentsResponse();
    }

}
