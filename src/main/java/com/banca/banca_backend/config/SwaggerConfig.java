package com.banca.banca_backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@PropertySource("classpath:swagger.properties")
@Configuration
public class SwaggerConfig {

    @Value("${swagger.api.version}")
    private String SWAGGER_API_VERSION;

    @Value("${swagger.license}")
    private String LICENSE_TEXT;

    @Value("${swagger.title}")
    private String TITLE;

    @Value("${swagger.description}")
    private String DESCRIPTION;

    @Value("${swagger.contact.name}")
    private String CONTACT_NAME;

    @Value("${swagger.contact.url}")
    private String CONTACT_URL;

    @Value("${swagger.contact.email}")
    private String CONTACT_EMAIL;

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(TITLE)
                .description(DESCRIPTION)
                .license(LICENSE_TEXT)
                .version(SWAGGER_API_VERSION)
                .contact(new Contact(CONTACT_NAME, CONTACT_URL, CONTACT_EMAIL))
                .build();
    }

    @Bean
    public Docket departmentApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.banca.banca_backend"))
                .paths(PathSelectors.regex("/.*"))
                .build();
    }

}
