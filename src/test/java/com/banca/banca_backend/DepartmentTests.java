package com.banca.banca_backend;

import com.banca.banca_backend.form.bean.ProvinceBean;
import com.banca.banca_backend.form.domain.Department;
import com.banca.banca_backend.form.repository.DepartmentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DepartmentTests {

    @Autowired
    DepartmentRepository departmentRepository;

    @Test
    public void createNewDepartments() {

        Department department = new Department();
        ProvinceBean provinceBean = null;
        List<String> districts = null;
        List<ProvinceBean> provinceBeans = null;

        // DEPARTMENT: PUNO
        provinceBeans = new ArrayList<>();

        provinceBean = new ProvinceBean();
        provinceBean.setName("AZÁNGARO");
        districts = new ArrayList<>();
        districts.add("ACHAYA");
        districts.add("ARAPA");
        districts.add("ASILLO");
        districts.add("AZANGARO");
        districts.add("CAMINACA");
        districts.add("CHUPA");
        districts.add("JOSE DOMINGO CHOQUEHUANCA");
        districts.add("MUÑANI");
        districts.add("POTONI");
        districts.add("SAMAN");
        districts.add("SAN ANTON");
        districts.add("SAN JOSE");
        districts.add("SAN JUAN DE SALINAS");
        districts.add("SANTIAGO DE PUPUJA");
        districts.add("TIRAPATA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("CARABAYA");
        districts = new ArrayList<>();
        districts.add("AJOYANI");
        districts.add("AYAPATA");
        districts.add("COASA");
        districts.add("CORANI");
        districts.add("CRUCERO");
        districts.add("ITUATA");
        districts.add("MACUSANI");
        districts.add("OLLACHEA");
        districts.add("SAN GABAN");
        districts.add("USICAYOS");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("CHUCUITO");
        districts = new ArrayList<>();
        districts.add("DESAGUADERO");
        districts.add("HUACULLANI");
        districts.add("JULI");
        districts.add("KELLUYO");
        districts.add("PISACOMA");
        districts.add("POMATA");
        districts.add("ZEPITA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("EL COLLAO");
        districts = new ArrayList<>();
        districts.add("CAPAZO");
        districts.add("CONDURIRI");
        districts.add("ILAVE");
        districts.add("PILCUYO");
        districts.add("SANTA ROSA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("HUANCANÉ");
        districts = new ArrayList<>();
        districts.add("COJATA");
        districts.add("HUANCANE");
        districts.add("HUATASANI");
        districts.add("INCHUPALLA");
        districts.add("PUSI");
        districts.add("ROSASPATA");
        districts.add("TARACO");
        districts.add("VILQUE CHICO");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("LAMPA");
        districts = new ArrayList<>();
        districts.add("CABANILLA");
        districts.add("CALAPUJA");
        districts.add("LAMPA");
        districts.add("NICASIO");
        districts.add("OCUVIRI");
        districts.add("PALCA");
        districts.add("PARATIA");
        districts.add("PUCARA");
        districts.add("SANTA LUCIA");
        districts.add("VILAVILA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("MELGAR");
        districts = new ArrayList<>();
        districts.add("ANTAUTA");
        districts.add("AYAVIRI");
        districts.add("CUPI");
        districts.add("LLALLI");
        districts.add("MACARI");
        districts.add("NUÑOA");
        districts.add("ORURILLO");
        districts.add("SANTA ROSA");
        districts.add("UMACHIRI");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("MOHO");
        districts = new ArrayList<>();
        districts.add("CONIMA");
        districts.add("HUAYRAPATA");
        districts.add("MOHO");
        districts.add("TILALI");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("PUNO");
        districts = new ArrayList<>();
        districts.add("ACORA");
        districts.add("AMANTANI");
        districts.add("ATUNCOLLA");
        districts.add("CAPACHICA");
        districts.add("CHUCUITO");
        districts.add("COATA");
        districts.add("HUATA");
        districts.add("MAÑAZO");
        districts.add("PAUCARCOLLA");
        districts.add("PICHACANI");
        districts.add("PLATERIA");
        districts.add("PUNO");
        districts.add("SAN ANTONIO");
        districts.add("TIQUILLACA");
        districts.add("VILQUE");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("SAN ANTONIO DE PUTINA");
        districts = new ArrayList<>();
        districts.add("ANANEA");
        districts.add("PEDRO VILCA APAZA");
        districts.add("PUTINA");
        districts.add("QUILCAPUNCU");
        districts.add("SINA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("SAN ROMÁN");
        districts = new ArrayList<>();
        districts.add("CABANA");
        districts.add("CABANILLAS");
        districts.add("CARACOTO");
        districts.add("JULIACA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("SANDIA");
        districts = new ArrayList<>();
        districts.add("ALTO INAMBARI");
        districts.add("CUYOCUYO");
        districts.add("LIMBANI");
        districts.add("PATAMBUCO");
        districts.add("PHARA");
        districts.add("QUIACA");
        districts.add("SAN JUAN DEL ORO");
        districts.add("SAN PEDRO DE PUTINA PUNCO");
        districts.add("SANDIA");
        districts.add("YANAHUAYA");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);


        provinceBean = new ProvinceBean();
        provinceBean.setName("YUNGUYO");
        districts = new ArrayList<>();
        districts.add("ANAPIA");
        districts.add("COPANI");
        districts.add("CUTURAPI");
        districts.add("OLLARAYA");
        districts.add("TINICACHI");
        districts.add("UNICACHI");
        districts.add("YUNGUYO");
        provinceBean.setDistricts(districts);
        provinceBeans.add(provinceBean);
        
        department.setName("PUNO");
        department.setProvinces(provinceBeans);

        departmentRepository.createNewDepartment(department);


        // DEPARTMENT: AMAZONAS
        department = new Department();
        department.setName("AMAZONAS");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: ÁNCASH
        department = new Department();
        department.setName("ÁNCASH");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: APURÍMAC
        department = new Department();
        department.setName("APURÍMAC");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: AREQUIPA
        department = new Department();
        department.setName("AREQUIPA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: AYACUCHO
        department = new Department();
        department.setName("AYACUCHO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: CAJAMARCA
        department = new Department();
        department.setName("CAJAMARCA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: CALLAO
        department = new Department();
        department.setName("CALLAO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: CUSCO
        department = new Department();
        department.setName("CUSCO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: HUANCAVELICA
        department = new Department();
        department.setName("HUANCAVELICA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: HUÁNUCO
        department = new Department();
        department.setName("HUÁNUCO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: ICA
        department = new Department();
        department.setName("ICA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: JUNÍN
        department = new Department();
        department.setName("JUNÍN");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: LA LIBERTAD
        department = new Department();
        department.setName("LA LIBERTAD");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: LAMBAYEQUE
        department = new Department();
        department.setName("LAMBAYEQUE");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: LIMA METROPOLITANA
        department = new Department();
        department.setName("LIMA METROPOLITANA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: LIMA
        department = new Department();
        department.setName("LIMA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: LORETO
        department = new Department();
        department.setName("LORETO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: MADRE DE DIOS
        department = new Department();
        department.setName("MADRE DE DIOS");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: MOQUEGUA
        department = new Department();
        department.setName("MOQUEGUA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: PASCO
        department = new Department();
        department.setName("PASCO");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: PIURA
        department = new Department();
        department.setName("PIURA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: SAN MARTÍN
        department = new Department();
        department.setName("SAN MARTÍN");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: TACNA
        department = new Department();
        department.setName("TACNA");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: TUMBES
        department = new Department();
        department.setName("TUMBES");
        departmentRepository.createNewDepartment(department);

        // DEPARTMENT: UCAYALI
        department = new Department();
        department.setName("UCAYALI");
        departmentRepository.createNewDepartment(department);

    }

    @Test
    public void getAllDepartments() {
        List<Department> departments = departmentRepository.getAllDepartments();
        System.out.println("SIZE: " + departments.size());
    }

}
