package com.banca.banca_backend;

import com.banca.banca_backend.form.domain.Person;
import com.banca.banca_backend.form.repository.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonTests {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void createNewPerson() {
        Person person = new Person();
        person.setName("Diego Rodrigo Huamanchahua de la Cruz");
        person.setBirthdate(getTimeInMillis(14, 10, 1991));
        person.setDepartment("PUNO");
        person.setProvince("CHUCUITO");
        person.setDistrict("ZEPITA");
        person.setAddress("Av. Las Magnolias 1124 (Alt. Plaza Vea)");
        personRepository.createNewPerson(person);
    }

    @Test
    public void getAllPersons() {
        List<Person> persons = personRepository.getAllPersons();
        System.out.println("SIZE: " + persons.size());
    }

    @Test
    public void updatePerson() {
        String name = "Diego Rodrigo Huamanchahua de la Cruz";

        Person person = new Person();
        person.setName("Diego Huamanchahua");
        person.setBirthdate(getTimeInMillis(10, 10, 1995));
        person.setDepartment("PUNO");
        person.setProvince("EL COLLAO");
        person.setDistrict("CAPAZO");
        person.setAddress("Av. La Molina 2055 (Alt. Casino de Policia)");

        personRepository.updatePerson(name, person);
    }

    @Test
    public void deletePerson() {
        String name = "Diego Huamanchahua";
        personRepository.deletePerson(name);
    }

    private Long getTimeInMillis(int date, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(year, month - 1, date);
        return calendar.getTimeInMillis();
    }

}
